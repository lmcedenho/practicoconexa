<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Conexa\PlanetCoordinate\Infrastructure\ListMovementsController;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ListMovementsApiController extends Controller
{
    /**
     * @var ListMovementsController
     */
    private $listMovementController;

    public function __construct(ListMovementsController $listMovementController)
    {
        $this->listMovementController = $listMovementController;
    }

    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     */
    public function __invoke(Request $request)
    {
        $listMovements = $this->listMovementController->__invoke($request);
        return response($listMovements, 201);
    }
}
