<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FinalPositionRequest;
use Conexa\PlanetCoordinate\Infrastructure\FinalPositionController;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FinalPositionApiController extends Controller
{
    /**
     * @var FinalPositionController
     */
    private $finalPositionController;

    public function __construct(FinalPositionController $finalPositionController)
    {
        $this->finalPositionController = $finalPositionController;
    }

    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     */
    public function __invoke(FinalPositionRequest $request)
    {
        try {
            $finalPosition = $this->finalPositionController->__invoke($request);
            return response($finalPosition, 201);
        } catch(\Exception $e) {
            return response($e->getMessage(), 422);
        }
    }
}
