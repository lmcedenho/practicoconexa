<?php

namespace App\Http\Requests;

use Conexa\PlanetCoordinate\Domain\Contracts\FinalPositionRawDataContract;
use Illuminate\Http\Request;

class FinalPositionRequest implements FinalPositionRawDataContract
{
    const INITIAL_POSITION = 'initial';
    const ITEMS = 'items';

    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getInitialPosition(): float
    {
        $initialPosition = $this->request->input(self::INITIAL_POSITION);
        return floatval($initialPosition[0] .','. $initialPosition[1]);
    }

    public function getItems(): array
    {
        return $this->request->input(self::ITEMS);
    }

    private function rules(): array
    {
        return [
            self::INITIAL_POSITION =>'required|array|between:2,2',
            self::ITEMS => 'required|array|min:5|max:7'
        ];
    }

    public function validate(): array
    {
        return $this->request->validate($this->rules());
    }
}
