<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $db = DB::table('movements');

        $movements = array(
            [
                'name' => 'up',
                'orientation' => 'height',
                'interval_units' => -1,
                'created_at' => Carbon::now()->format("Y-m-d H:i:s")
            ],
            [
                'name' => 'down',
                'orientation' => 'height',
                'interval_units' => 1,
                'created_at' => Carbon::now()->format("Y-m-d H:i:s")
            ],
            [
                'name' => 'right',
                'orientation' => 'width',
                'interval_units' => 1,
                'created_at' => Carbon::now()->format("Y-m-d H:i:s")
            ],
            [
                'name' => 'left',
                'orientation' => 'width',
                'interval_units' => -1,
                'created_at' => Carbon::now()->format("Y-m-d H:i:s")
            ]
        );

        foreach ($movements as $movement) {
            if (! $db->where( 'name', '=', $movement['name'])->exists()) {
                $db->Insert($movement);
            }
        }
    }
}
