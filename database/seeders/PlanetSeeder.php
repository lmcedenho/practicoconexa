<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlanetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $db = DB::table('planets');

        $planets = array([
            'name' => 'earth',
            'height' => '3',
            'width' => '3',
            'height_interval' => '1',
            'width_interval' => '0.1',
            'created_at' => Carbon::now()->format("Y-m-d H:i:s")
        ]);

        foreach ($planets as $planet) {
            if (! $db->where( 'name', '=', $planet['name'])->exists()) {
                $db->Insert($planet);
            }
        }
    }
}
