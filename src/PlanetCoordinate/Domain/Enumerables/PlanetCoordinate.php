<?php

namespace Conexa\PlanetCoordinate\Domain\Enumerables;

class PlanetCoordinate
{
    const DEFAULT_PLANET_NAME = 'earth';
    const PLANET_MOVEMENT_ORIENTATION_HEIGHT = 'height';
    const PLANET_MOVEMENT_ORIENTATION_WIDTH = 'width';
}
