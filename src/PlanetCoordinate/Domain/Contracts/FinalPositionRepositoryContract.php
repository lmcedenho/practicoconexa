<?php

namespace Conexa\PlanetCoordinate\Domain\Contracts;

use Conexa\PlanetCoordinate\Domain\Entities\Planet;

interface FinalPositionRepositoryContract
{
    public function finalPosition(Planet $planet, FinalPositionRawDataContract $rawData);
}
