<?php

namespace Conexa\PlanetCoordinate\Domain\Contracts;

interface MovementRepositoryContract
{
    public function listRandomMovements(): ?array;
}
