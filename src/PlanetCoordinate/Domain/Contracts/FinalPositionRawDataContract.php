<?php

namespace Conexa\PlanetCoordinate\Domain\Contracts;

interface FinalPositionRawDataContract
{
    public function getInitialPosition(): float;

    public function getItems(): array;

    public function validate(): array;
}
