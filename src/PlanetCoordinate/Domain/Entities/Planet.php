<?php

namespace Conexa\PlanetCoordinate\Domain\Entities;

final class Planet
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $height;
    /**
     * @var int
     */
    private $width;
    /**
     * @var float
     */
    private $heightInterval;
    /**
     * @var float
     */
    private $width_interval;

    public function __construct(string $name, int $height, int $width, float $heightInterval, float $width_interval)
    {
        $this->name = $name;
        $this->height = $height;
        $this->width = $width;
        $this->heightInterval = $heightInterval;
        $this->width_interval =$width_interval;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return float
     */
    public function getHeightInterval(): float
    {
        return $this->heightInterval;
    }

    /**
     * @return float
     */
    public function getWidthInterval(): float
    {
        return $this->width_interval;
    }
}
