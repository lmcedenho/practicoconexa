<?php

namespace Conexa\PlanetCoordinate\Domain\Entities;

class Movement
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $orientation;
    /**
     * @var int
     */
    private $interval_units;

    public function __construct(string $name, string $orientation, int $interval_units)
    {
        $this->name = $name;
        $this->orientation = $orientation;
        $this->interval_units = $interval_units;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getOrientation(): string
    {
        return $this->orientation;
    }

    /**
     * @return int
     */
    public function getIntervalUnits(): int
    {
        return $this->interval_units;
    }


}
