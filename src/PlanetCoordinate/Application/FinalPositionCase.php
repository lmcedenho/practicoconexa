<?php

namespace Conexa\PlanetCoordinate\Application;

use Conexa\PlanetCoordinate\Domain\Contracts\FinalPositionRawDataContract;
use Conexa\PlanetCoordinate\Domain\Contracts\FinalPositionRepositoryContract;
use Conexa\PlanetCoordinate\Domain\Entities\Planet;

final class FinalPositionCase
{
    /**
     * @var FinalPositionRepositoryContract
     */
    private $repository;

    public function __construct(FinalPositionRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    public function finalPosition(Planet $planet, FinalPositionRawDataContract $rawData)
    {
        return $this->repository->finalPosition($planet, $rawData);
    }
}
