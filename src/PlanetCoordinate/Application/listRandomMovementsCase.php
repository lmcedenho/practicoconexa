<?php

namespace Conexa\PlanetCoordinate\Application;

use Conexa\PlanetCoordinate\Domain\Contracts\MovementRepositoryContract;
use Conexa\PlanetCoordinate\Infrastructure\Repositories\EloquentMovementRepository;

final class listRandomMovementsCase
{
    /**
     * @var EloquentMovementRepository
     */
    private $repository;

    public function __construct(MovementRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    public function listRandomMovements(): ?array
    {
        return $this->repository->listRandomMovements();
    }
}
