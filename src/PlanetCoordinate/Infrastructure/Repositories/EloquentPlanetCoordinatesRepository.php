<?php

namespace Conexa\PlanetCoordinate\Infrastructure\Repositories;

use App\Models\Planet as EloquentPlanetModel;
use App\Models\Movement as EloquentMovementModel;
use Conexa\PlanetCoordinate\Domain\Contracts\FinalPositionRawDataContract;
use Conexa\PlanetCoordinate\Domain\Contracts\FinalPositionRepositoryContract;
use Conexa\PlanetCoordinate\Domain\Entities\Movement;
use Conexa\PlanetCoordinate\Domain\Entities\Planet;
use Conexa\PlanetCoordinate\Domain\Enumerables\PlanetCoordinate;

final class EloquentPlanetCoordinatesRepository implements FinalPositionRepositoryContract
{
    /**
     * @var EloquentPlanetModel
     */
    private $eloquentPlanetModel;
    /**
     * @var EloquentMovementModel
     */
    private $eloquentMovementModel;

    public function __construct()
    {
        $this->eloquentPlanetModel = new EloquentPlanetModel;
        $this->eloquentMovementModel = new EloquentMovementModel;
    }

    public function findPlanetByName(string $name): Planet
    {
        $eloquentPlanet = $this->eloquentPlanetModel->where('name', '=', $name)->first();

        if ( !$eloquentPlanet ) {
            throw new \Exception('Planet Not Found');
        }

        return new Planet($eloquentPlanet->name, $eloquentPlanet->height, $eloquentPlanet->width, $eloquentPlanet->height_interval, $eloquentPlanet->width_interval);
    }

    public function finalPosition(Planet $planet, FinalPositionRawDataContract $rawData)
    {
        $currentPosition = $rawData->getInitialPosition();
        $movements = $this->getMovementsByNames($rawData->getItems());

        $validMovements = [];

        foreach($rawData->getItems() as $movement)
        {
            $eloquentMovement = collect($movements)->where('name', '=', $movement)->first();

            if (! $eloquentMovement ) {
                throw new \Exception('Movement Not Found');
            }

            $movement = new Movement($eloquentMovement->name, $eloquentMovement->orientation, $eloquentMovement->interval_units);

            $newPosition = $this->getNewPosition($planet, $movement, $currentPosition);
            if ( $newPosition ) {
                $currentPosition = $newPosition;
                $validMovements[] = $newPosition;
            }
        }

        return [
          'movements' => $validMovements
        ];
    }

    private function getMovementsByNames(array $names)
    {
        return $this->eloquentMovementModel->whereIn('name', $names)->get();
    }

    private function getNewPosition(Planet $planet, Movement $movement, float $currentPosition): ?string
    {
        $coordinate = $currentPosition;

        switch ($movement->getOrientation()) {
            case PlanetCoordinate::PLANET_MOVEMENT_ORIENTATION_HEIGHT:
                $coordinate = floatval($currentPosition + ($planet->getHeightInterval() * $movement->getIntervalUnits()));
                break;
            case PlanetCoordinate::PLANET_MOVEMENT_ORIENTATION_WIDTH:
                $coordinate = floatval($currentPosition + ($planet->getWidthInterval() * $movement->getIntervalUnits()));
                break;
        }

        return $this->validCoordinate($coordinate, $planet);
    }

    /**
     * @param float $coordinate
     * @param Planet $planet
     * @return string|null return valid coordinate movement for current planet
     */
    private function validCoordinate(float $coordinate, Planet $planet): ?string
    {
        $planetCoordinates = $this->allPlanetCoordinates($planet);
        if (!in_array($coordinate, $planetCoordinates)) {
            return null;
        }
        return number_format($coordinate, 1);
    }

    /**
     * @param Planet $planet
     * @return int[] returns all possible coordinates based on the intervals, width and length of the planet
     */
    private function allPlanetCoordinates(Planet $planet): array
    {
        $widthCoordinates = [0];

        for( $w = 1; $w < $planet->getWidth(); $w++ ) {
            $widthCoordinates[] = end($widthCoordinates) + $planet->getWidthInterval();
        }

        $baseCoordinates = $widthCoordinates;
        for( $h = 1; $h < $planet->getHeight(); $h++ ) {
            foreach ($baseCoordinates as $coordinate) {
                $heightCoordinates[] = $coordinate + $planet->getHeightInterval();
            }
            $baseCoordinates = $heightCoordinates ?? [];
        }

        return array_merge($widthCoordinates, $heightCoordinates ?? []);
    }
}
