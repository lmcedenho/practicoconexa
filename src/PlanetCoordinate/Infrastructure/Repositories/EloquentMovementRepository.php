<?php

namespace Conexa\PlanetCoordinate\Infrastructure\Repositories;

use App\Models\Movement as EloquentMovementModel;
use Conexa\PlanetCoordinate\Domain\Contracts\MovementRepositoryContract;

final class EloquentMovementRepository implements MovementRepositoryContract
{
    const MIN_MOVEMENTS = 5;
    const MAX_MOVEMENTS = 7;
    /**
     * @var EloquentMovementModel
     */
    private $eloquentMovementModel;

    public function __construct()
    {
        $this->eloquentMovementModel = new EloquentMovementModel;
    }

    public function listRandomMovements(): ?array
    {
        $listMovements = $this->getAllMovementsNames();
        $countRandomMovements = $this->getCountMovements();
        $totalMovementsRows = $listMovements->count();

        if( $totalMovementsRows <  $countRandomMovements) {
            return $this->autoFillMovements($totalMovementsRows, $countRandomMovements, $listMovements->toArray());
        }

        return $listMovements->random($countRandomMovements)->toArray();
    }

    private function getCountMovements(): int
    {
        return rand(self::MIN_MOVEMENTS, self::MAX_MOVEMENTS);;
    }

    private function getAllMovementsNames()
    {
        return $this->eloquentMovementModel->pluck('name');
    }

    private function autoFillMovements(int $totalMovementsRows, int $countRandomMovements, array $listMovements): array
    {
        for( $i = $totalMovementsRows; $i < $countRandomMovements; $i++) {
            $randMovement = array($listMovements[array_rand($listMovements)]);
            $listMovements = array_merge($listMovements, $randMovement);
        }

        shuffle($listMovements);

        return $listMovements;
    }
}
