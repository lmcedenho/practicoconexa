<?php

namespace Conexa\PlanetCoordinate\Infrastructure;

use Conexa\PlanetCoordinate\Application\listRandomMovementsCase;
use Conexa\PlanetCoordinate\Infrastructure\Repositories\EloquentMovementRepository;
use Illuminate\Http\Request;

final class ListMovementsController
{
    /**
     * @var EloquentMovementRepository
     */
    private $repository;

    public function __construct(EloquentMovementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(Request $request): ?array
    {
        $listRandomMovementsCase = new listRandomMovementsCase($this->repository);
        return $listRandomMovementsCase->listRandomMovements();
    }
}
