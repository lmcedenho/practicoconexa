<?php

namespace Conexa\PlanetCoordinate\Infrastructure;

use Conexa\PlanetCoordinate\Application\FinalPositionCase;
use Conexa\PlanetCoordinate\Domain\Contracts\FinalPositionRawDataContract;
use Conexa\PlanetCoordinate\Domain\Enumerables\PlanetCoordinate;
use Conexa\PlanetCoordinate\Infrastructure\Repositories\EloquentPlanetCoordinatesRepository;

final class FinalPositionController
{
    /**
     * @var EloquentPlanetCoordinatesRepository
     */
    private $repository;

    public function __construct(EloquentPlanetCoordinatesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(FinalPositionRawDataContract $rawData)
    {
        $rawData->validate();
        $planet = $this->repository->findPlanetByName(PlanetCoordinate::DEFAULT_PLANET_NAME);

        $finalPositionCase = new FinalPositionCase($this->repository);

        return $finalPositionCase->finalPosition($planet, $rawData);
    }
}
